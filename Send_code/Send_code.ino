#include <IRremote.h>
#include <IRremoteInt.h>
 
IRsend irsend;
int pinButton_hdmi = 2;
int pinButton_pwr = 4;
int valor_button_hdmi; 
int valor_button_pwr; 
 
void setup() {
  Serial.begin(9600);
  pinMode(pinButton_hdmi,INPUT_PULLUP);
  pinMode(pinButton_pwr,INPUT_PULLUP);
  
}
 
void loop() {
  valor_button_hdmi = digitalRead(pinButton_hdmi);
  valor_button_pwr=digitalRead(pinButton_pwr);
  
  if(valor_button_pwr == LOW)
  {
    irsend.sendNEC(0x20DF10EF,32);
    Serial.println("Enviado");
    delay(100);
  }  
  if(valor_button_hdmi == LOW)
  {
    irsend.sendNEC(0x20DFD02F,32);
    Serial.println("Enviado");
    delay(100);
  }
}
